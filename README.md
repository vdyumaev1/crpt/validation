Microservice for validation Document request.

Only controller layer used here because as I think it's the best way to find errors as soon as possible and spring validation API gives us this possibility. Also it can return the list of all errors in the document. 
