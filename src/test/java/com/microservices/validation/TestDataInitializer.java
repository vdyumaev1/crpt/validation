package com.microservices.validation;

import com.microservices.validation.model.Document;
import com.microservices.validation.model.Product;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class TestDataInitializer {

    private static final int SELLER_MAX_LENGTH = 9;
    private static final int CUSTOMER_MAX_LENGTH = 9;
    private static final int CODE_MAX_LENGTH = 13;


    private static final String NAME = "someName";
    private static final String BLANK = "     ";


    public static Product createProduct(String code, String name) {
        return Product.builder()
                .code(code)
                .name(name)
                .build();
    }

    public static Document createDocument(String seller, String customer, List<Product> products) {
        return Document.builder()
                .seller(seller)
                .customer(customer)
                .products(products)
                .build();
    }

    public static Document createCorrectDocument() {
        return createDocument(generateField(SELLER_MAX_LENGTH),
                generateField(CUSTOMER_MAX_LENGTH),
                Collections.singletonList(createProduct(generateField(CODE_MAX_LENGTH), NAME)));
    }

    public static Document createSellerOverLimitDocument() {
        return createDocument(generateField(SELLER_MAX_LENGTH + 1),
                generateField(CUSTOMER_MAX_LENGTH),
                Collections.singletonList(createProduct(generateField(CODE_MAX_LENGTH), NAME)));
    }

    public static Document createCustomerOverLimitDocument() {
        return createDocument(generateField(SELLER_MAX_LENGTH),
                generateField(CUSTOMER_MAX_LENGTH + 1),
                Collections.singletonList(createProduct(generateField(CODE_MAX_LENGTH), NAME)));
    }

    public static Document createSellerBlankDocument() {
        return createDocument(BLANK,
                generateField(CUSTOMER_MAX_LENGTH),
                Collections.singletonList(createProduct(generateField(CODE_MAX_LENGTH), NAME)));
    }

    public static Document createCustomerBlankDocument() {
        return createDocument(generateField(SELLER_MAX_LENGTH),
                BLANK,
                Collections.singletonList(createProduct(generateField(CODE_MAX_LENGTH), NAME)));
    }

    public static Document createSellerNullDocument() {
        return createDocument(null,
                generateField(CUSTOMER_MAX_LENGTH),
                Collections.singletonList(createProduct(generateField(CODE_MAX_LENGTH), NAME)));
    }

    public static Document createCustomerNullDocument() {
        return createDocument(generateField(SELLER_MAX_LENGTH),
                null,
                Collections.singletonList(createProduct(generateField(CODE_MAX_LENGTH), NAME)));
    }

    public static Document createCodeOverLimitDocument() {
        return createDocument(generateField(SELLER_MAX_LENGTH),
                generateField(CUSTOMER_MAX_LENGTH),
                Collections.singletonList(createProduct(generateField(CODE_MAX_LENGTH + 1), NAME)));
    }

    public static Document createCodeBlankDocument() {
        return createDocument(generateField(SELLER_MAX_LENGTH),
                generateField(CUSTOMER_MAX_LENGTH),
                Collections.singletonList(createProduct(BLANK, NAME)));
    }

    public static Document createCodeNullDocument() {
        return createDocument(generateField(SELLER_MAX_LENGTH),
                generateField(CUSTOMER_MAX_LENGTH),
                Collections.singletonList(createProduct(null, NAME)));
    }

    public static Document createNameBlankDocument() {
        return createDocument(generateField(SELLER_MAX_LENGTH),
                generateField(CUSTOMER_MAX_LENGTH),
                Collections.singletonList(createProduct(generateField(CODE_MAX_LENGTH), BLANK)));
    }

    public static Document createNameNullDocument() {
        return createDocument(generateField(SELLER_MAX_LENGTH),
                generateField(CUSTOMER_MAX_LENGTH),
                Collections.singletonList(createProduct(generateField(CODE_MAX_LENGTH), null)));
    }

    public static Document createProductsEmptyListDocument() {
        return createDocument(generateField(SELLER_MAX_LENGTH),
                generateField(CUSTOMER_MAX_LENGTH),
                Collections.emptyList());
    }

    public static Document createProductsNullDocument() {
        return createDocument(generateField(SELLER_MAX_LENGTH),
                generateField(CUSTOMER_MAX_LENGTH),
                null);
    }

    public static Document createAllFieldsErrorsDocument() {
        return createDocument(generateField(SELLER_MAX_LENGTH + 1),
                BLANK,
                Arrays.asList(createProduct(generateField(CODE_MAX_LENGTH + 1), NAME), createProduct(BLANK, null)));
    }


    // private methods
    private static String generateField(int maxLength) {
        return StringUtils.repeat("a", maxLength);
    }


}
