package com.microservices.validation.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static com.microservices.validation.TestDataInitializer.*;
import static com.microservices.validation.constants.ValidationClientPaths.VALIDATION_PATH;
import static com.microservices.validation.helper.GenericTestHelper.toJson;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ValidationControllerTest {

    private static final String CORRECT_RESPONSE = "document is OK";

    @Autowired
    public MockMvc mockMvc;

    @Test
    void testValidate_SunnyCase() throws Exception {
        String result = mockMvc.perform(post(VALIDATION_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(createCorrectDocument())))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        assertThat(result).isEqualTo(CORRECT_RESPONSE);
    }

    @Test
    void testValidate_SellerOverlimit() throws Exception {
        String result = mockMvc.perform(post(VALIDATION_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(createSellerOverLimitDocument())))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        assertThat(result).contains("[Seller field should have less or equals than 9 symbols]");
    }

    @Test
    void testValidate_SellerBlank() throws Exception {
        String result = mockMvc.perform(post(VALIDATION_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(createSellerBlankDocument())))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        assertThat(result).contains("[Seller field should be not blank]");
    }

    @Test
    void testValidate_SellerNull() throws Exception {
        String result = mockMvc.perform(post(VALIDATION_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(createSellerNullDocument())))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        assertThat(result).contains("[Seller field should be not blank]");
    }

    @Test
    void testValidate_CustomerOverlimit() throws Exception {
        String result = mockMvc.perform(post(VALIDATION_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(createCustomerOverLimitDocument())))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        assertThat(result).contains("[Customer field should have less or equals than 9 symbols]");
    }

    @Test
    void testValidate_CustomerBlank() throws Exception {
        String result = mockMvc.perform(post(VALIDATION_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(createCustomerBlankDocument())))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        assertThat(result).contains("[Customer field should be not blank]");
    }

    @Test
    void testValidate_CustomerNull() throws Exception {
        String result = mockMvc.perform(post(VALIDATION_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(createCustomerNullDocument())))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        assertThat(result).contains("[Customer field should be not blank]");
    }

    @Test
    void testValidate_CodeOverLimit() throws Exception {
        String result = mockMvc.perform(post(VALIDATION_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(createCodeOverLimitDocument())))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        assertThat(result).contains("[Product.code should have 13 or less symbols]");
    }

    @Test
    void testValidate_CodeBlank() throws Exception {
        String result = mockMvc.perform(post(VALIDATION_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(createCodeBlankDocument())))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        assertThat(result).contains("[Product.code field should be not blank]");
    }

    @Test
    void testValidate_CodeNull() throws Exception {
        String result = mockMvc.perform(post(VALIDATION_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(createCodeNullDocument())))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        assertThat(result).contains("[Product.code field should be not blank]");
    }

    @Test
    void testValidate_NameBlank() throws Exception {
        String result = mockMvc.perform(post(VALIDATION_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(createNameBlankDocument())))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        assertThat(result).contains("[Product.name field should be not blank]");
    }

    @Test
    void testValidate_NameNull() throws Exception {
        String result = mockMvc.perform(post(VALIDATION_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(createNameNullDocument())))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        assertThat(result).contains("[Product.name field should be not blank]");
    }

    @Test
    void testValidate_ProductsEmptyList() throws Exception {
        String result = mockMvc.perform(post(VALIDATION_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(createProductsEmptyListDocument())))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        assertThat(result).contains("[products list should not be empty or null]");
    }

    @Test
    void testValidate_ProductsNull() throws Exception {
        String result = mockMvc.perform(post(VALIDATION_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(createProductsNullDocument())))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        assertThat(result).contains("[products list should not be empty or null]");
    }

    @Test
    void testValidate_AllErrorsTogether() throws Exception {
        String result = mockMvc.perform(post(VALIDATION_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(createAllFieldsErrorsDocument())))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        assertThat(result).contains("Field error in object 'document' on field 'seller': rejected value [aaaaaaaaaa];")
                .contains("Field error in object 'document' on field 'customer': rejected value [     ];")
                .contains("Field error in object 'document' on field 'products[0].code': rejected value [aaaaaaaaaaaaaa];")
                .contains("Field error in object 'document' on field 'products[1].code': rejected value [     ];")
                .contains("Field error in object 'document' on field 'products[1].name': rejected value [null];");
    }

}