package com.microservices.validation.controller;

import com.microservices.validation.model.Document;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import static com.microservices.validation.constants.ValidationClientPaths.VALIDATION_PATH;

@RestController
@Slf4j
public class ValidationController {

    @PostMapping(value = VALIDATION_PATH)
    public String validate(@RequestBody @Valid @NotNull Document document) {
        log.debug("Document: {}", document);
        return "document is OK";
    }
}
