package com.microservices.validation.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class GlobalControllerAdvice {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<String> onError(MethodArgumentNotValidException error) {
        log.debug(error.getMessage());

        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(error.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> onError(Exception error) {
        log.debug(error.getMessage());

        return ResponseEntity
                .status(HttpStatus.BAD_GATEWAY)
                .body(error.getMessage());
    }

}
