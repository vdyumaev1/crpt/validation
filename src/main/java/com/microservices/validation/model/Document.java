package com.microservices.validation.model;

import lombok.Builder;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Builder
public class Document {

    @NotBlank(message = "Seller field should be not blank" )
    @Size(max = 9, message = "Seller field should have less or equals than 9 symbols")
    private String seller;

    @NotBlank(message = "Customer field should be not blank" )
    @Size(max = 9, message = "Customer field should have less or equals than 9 symbols")
    private String customer;

    @Valid
    @NotEmpty(message = "products list should not be empty or null")
    private List<Product> products;
}
