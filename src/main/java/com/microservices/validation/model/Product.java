package com.microservices.validation.model;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Builder
public class Product {

    @NotBlank(message = "Product.name field should be not blank")
    private String name;

    @NotBlank(message = "Product.code field should be not blank")
    @Size(max = 13, message = "Product.code should have 13 or less symbols")
    private String code;
}
