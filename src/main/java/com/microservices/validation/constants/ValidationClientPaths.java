package com.microservices.validation.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ValidationClientPaths {

    public static final String BASE_PATH = "/api/v0";//NOSONAR
    public static final String VALIDATION_PATH = BASE_PATH +  "/validation";
}
